
public enum SimulationType {
	TIME("Time"), 
	FRAME("Frame");

	private final String name;

	private SimulationType(String s) {
		name = s;
	}

	public String toString() {
		return name;
	}
}
