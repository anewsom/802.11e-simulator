
import java.io.IOException;
import java.text.DecimalFormat;

public class Logger {

	private static Logger instance;
	
	private ConfigReader config;
	private DecimalFormat dfm;
	
	public static Logger getInstance() {
		if (instance == null)
			instance = new Logger();

		return instance;
	}

	private Logger() {
		try {
			config = ConfigReader.getInstance();
			dfm = new DecimalFormat("0.00000");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void close() {

	}
	
	/***
	 * Generate report from data and display on console
	 * 
	 * @param numNode
	 * @param lambda
	 * @param maxBuffer
	 * @throws IOException
	 */
	public void printLog() {
		
		System.out.println("\r\n------------------------------");
		
		SimulationType simulation_type = config.getSimulationType();
		DistributionType distribution_type = config.getDistributionType();
		int end_condition = config.getEndCondition();
		double rate = config.getRate();
		
		System.out.println("Parameters:");
		System.out.println("\tSimulation Type: " + simulation_type);
		
		if (simulation_type == SimulationType.TIME)
			System.out.println("\tEnd condition after: " + end_condition + " second");
		else if (simulation_type == SimulationType.FRAME)
			System.out.println("\tEnd condition after: " + end_condition + " frame");
		
		if (distribution_type == DistributionType.NEDT)
			System.out.println("\tDistribution function: Negative Exponential Distribution");
		else if (distribution_type == DistributionType.PDT)
			System.out.println("\tDistribution function: Pareto Distribution");
		
		System.out.println("\tRate: "+rate);
		
		int normalMaxBuffer = config.getNormalBufferSize();
		int normalNodeNumber = config.getNormalNodeNumber();
		double normalLambda = config.getNormalLambda();
		int normalPriorityType = config.getNormalPriorityType();
		
		System.out.println("------------------------------");
		System.out.println("Normal server's configuration");
		System.out.println("\tLambda: " + normalLambda);
		System.out.println("\tNumber of Nodes: " + normalNodeNumber);
		System.out.println("\tMaximum buffer size: " + normalMaxBuffer);
		if (normalPriorityType == -1) {
			System.out.println("\tPriority Type: random");
		} else {
			EventACType normalEventType = EventACType.values()[normalPriorityType];
			System.out.println("\tPriority Type: " + normalEventType);
		}
		
		
		int qosMaxBuffer = config.getQosBufferSize();
		int qosNodeNumber = config.getQosNodeNumber();
		double qosLambda = config.getQosLambda();
		int qosPriorityType = config.getQosPriorityType();
		
		System.out.println("------------------------------");
		System.out.println("QoS server's configuration");
		System.out.println("\tLambda: " + qosLambda);
		System.out.println("\tNumber of Nodes: " + qosNodeNumber);
		System.out.println("\tMaximum buffer size: " + qosMaxBuffer);
		if (qosPriorityType == -1) {
			System.out.println("\tPriority Type: random");
		} else {
			EventACType qosEventType = EventACType.values()[qosPriorityType];
			System.out.println("\tPriority Type: " + qosEventType);
		}
		
		int monitorMaxBuffer = config.getMonitorBufferSize();
		int monitorNodeNumber = config.getMonitorNodeNumber();	
		double monitorLambda = config.getMonitorLambda();
		int monitorPriorityType = config.getMonitorPriorityType();
		
		System.out.println("------------------------------");
		System.out.println("Monitor server's configuration");
		System.out.println("\tLambda: " + monitorLambda);
		System.out.println("\tNumber of Nodes: " + monitorNodeNumber);
		System.out.println("\tMaximum buffer size: " + monitorMaxBuffer);
		if (monitorPriorityType == -1) {
			System.out.println("\tPriority Type: random");
		} else {
			EventACType monitorEventType = EventACType.values()[monitorPriorityType];
			System.out.println("\tPriority Type: " + monitorEventType);
		}
		
		System.out.println("------------------------------");
	
		System.out.println("Average queue size:");
		System.out.println("Normal server");
		
		for(int i = 0; i < normalNodeNumber; i++) {
			Server s = (Server) SimMain.servers[i];
			System.out.println("Server "+s.NODE+"'s average queue size = "+dfm.format(s.totalQueLengthOverTime/SimMain.runningTime));
		}
		
		System.out.println("QoS server");
		
		for(int i = normalNodeNumber; i < normalNodeNumber + qosNodeNumber; i++) {
			Server tmp = (Server) SimMain.servers[i];
			if (tmp instanceof ServerQoS) {
				ServerQoS qos = (ServerQoS) tmp;
				System.out.println("Server "+qos.NODE+"'s average queue size = " + dfm.format((qos.totalQueLengthOverTime/4)/SimMain.runningTime));
			}
		}
		
		System.out.println("Monitor server");
		
		for(int i = normalNodeNumber + qosNodeNumber; i < SimMain.servers.length; i++) {
			Server tmp = (Server) SimMain.servers[i];
			if (tmp instanceof ServerQoS) {
				ServerQoS monitor = (ServerQoS) tmp;
				System.out.println("Server "+monitor.NODE+"'s average queue size = " + dfm.format((monitor.totalQueLengthOverTime/4)/SimMain.runningTime));
			}
		}
		
		System.out.println("------------------------------");
		
		System.out.println("Throughput:");
		System.out.println("Normal server");
		
		for(int i = 0; i < normalNodeNumber; i++) {
			Server s = (Server) SimMain.servers[i];
			double throughput= s.bytesTransmittedSuccessfully/SimMain.runningTime;
			System.out.println("Server "+s.NODE+"'s throughput = " + throughput + " bps");
		}
		
		System.out.println("QoS server");
		
		for(int i = normalNodeNumber; i < normalNodeNumber + qosNodeNumber; i++) {
			Server tmp = (Server) SimMain.servers[i];
			if (tmp instanceof ServerQoS) {
				ServerQoS qos = (ServerQoS) tmp;
				double throughput= qos.getBytesTransmittedSuccessfully()/SimMain.runningTime;
				System.out.println("Server "+qos.NODE+"'s throughput = " + throughput + " bps");
			}
		}
		
		System.out.println("Monitor server");
		
		for(int i = normalNodeNumber + qosNodeNumber; i < SimMain.servers.length; i++) {
			Server tmp = (Server) SimMain.servers[i];
			if (tmp instanceof ServerQoS) {
				ServerQoS monitor = (ServerQoS) tmp;
				double throughput= monitor.getBytesTransmittedSuccessfully()/SimMain.runningTime;
				System.out.println("Server "+monitor.NODE+"'s throughput = " + throughput + " bps");
			}
		}
		
		System.out.println("------------------------------");
		
		System.out.println("Average Frame Delay:");
		
		System.out.println("Normal server");
		
		for(int i = 0; i < normalNodeNumber; i++) {
			Server s = (Server) SimMain.servers[i];
			
			double averageFrameDelay = s.packetDelay/s.framesTransmittedSuccessfully;
			System.out.println("Server "+s.NODE+"'s frame delay = " + averageFrameDelay + " seconds");
		}
		
		System.out.println("QoS server");
		
		for(int i = normalNodeNumber; i < normalNodeNumber + qosNodeNumber; i++) {
			Server tmp = (Server) SimMain.servers[i];
			if (tmp instanceof ServerQoS) {
				ServerQoS qos = (ServerQoS) tmp;
				
				double averageFrameDelay = qos.getPacketDelay()/qos.getFramesTransmittedSuccessfully();
				System.out.println("Server "+qos.NODE+"'s frame delay = " + averageFrameDelay + " seconds");
			}
		}
		
		System.out.println("Monitor server");
		
		for(int i = normalNodeNumber + qosNodeNumber; i < SimMain.servers.length; i++) {
			Server tmp = (Server) SimMain.servers[i];
			if (tmp instanceof ServerQoS) {
				ServerQoS monitor = (ServerQoS) tmp;
				
				double averageFrameDelay = monitor.getPacketDelay()/monitor.getFramesTransmittedSuccessfully();
				System.out.println("Server "+monitor.NODE+"'s frame delay = " + averageFrameDelay + " seconds");
			}
		}
		
		System.out.println("------------------------------");
		System.out.println("Network Overview");
		double total_running_time = SimMain.runningTime;
		double throughput= SimMain.bytesTransmittedSuccessfully/SimMain.runningTime;
		double averageFrameDelay = SimMain.totalPacketDelay/SimMain.framesTransmittedSuccessfully;
		
		int totalFramesDropped=0;
		for(Server s : SimMain.servers) {
			if (s instanceof ServerQoS) {
				ServerQoS qos = (ServerQoS)s;
				totalFramesDropped += qos.getTotalFramesDropped();
			} else {
				totalFramesDropped += s.framesDropped;
			}
		}
		
		double frameLossRate = totalFramesDropped/SimMain.runningTime;
		
		
		System.out.println("Total running time = " + total_running_time);
		System.out.println("Network Throughput = " + throughput + " bps");
		System.out.println("Average Frame Delay = " + averageFrameDelay + " seconds");
		System.out.println("Frame Loss Rate = " + frameLossRate + " frames/sec");	
		System.out.println("Number of Collisions = " + SimMain.numberOfCol);
		System.out.println("------------------------------");
	}

	public void printExcelLog(double lambda) {
		
		double throughput= SimMain.bytesTransmittedSuccessfully/SimMain.runningTime;
		
		int totalFramesDropped=0;
		for(Server s : SimMain.servers) {
			if (s instanceof ServerQoS){
				ServerQoS qos = (ServerQoS)s;
				totalFramesDropped += qos.getTotalFramesDropped();
			} else {
				totalFramesDropped += s.framesDropped;
			}
		}
			
		
		double frameLossRate = totalFramesDropped/SimMain.runningTime;
		
		double averageFrameDelay = SimMain.totalPacketDelay/SimMain.framesTransmittedSuccessfully;
		
		System.out.print(lambda + "\t" + dfm.format(throughput) + "\t");
		System.out.print(dfm.format(frameLossRate) + "\t" + dfm.format(averageFrameDelay));
		for(Server s : SimMain.servers) {
			if (s instanceof ServerQoS) {
				ServerQoS qos = (ServerQoS)s;
				System.out.print("\t" + qos.numberOfAC_BK );
				System.out.print("\t" + qos.numberOfAC_BE );
				System.out.print("\t" + qos.numberOfAC_VI );
				System.out.print("\t" + qos.numberOfAC_VO );
				
				System.out.print("\t" + dfm.format(qos.ACBKQueueLengthOverTime/SimMain.runningTime) );
				System.out.print("\t" + dfm.format(qos.getACBKFramesDropped()/SimMain.runningTime) );
				
				System.out.print("\t" + dfm.format(qos.ACBEQueueLengthOverTime/SimMain.runningTime) );
				System.out.print("\t" + dfm.format(qos.getACBEFramesDropped()/SimMain.runningTime) );
				
				System.out.print("\t" + dfm.format(qos.ACVIQueueLengthOverTime/SimMain.runningTime) );
				System.out.print("\t" + dfm.format(qos.getACVIFramesDropped()/SimMain.runningTime) );
				
				System.out.print("\t" + dfm.format(qos.ACVOQueueLengthOverTime/SimMain.runningTime) );
				System.out.print("\t" + dfm.format(qos.getACVOFramesDropped()/SimMain.runningTime) );
			} else {
				
			}
		}
		System.out.print("\r\n");
	}

}
