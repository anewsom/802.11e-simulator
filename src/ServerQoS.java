public class ServerQoS extends Server {

	public final String NODE_TYPE = "ServerQoS";

	private EDCAmodule edcaModule;

	// private double rate;

	// node number of this server
	// public final int NODE;

	// used for generate report
	public double usedTime;
	public double timeAtLastEvent;
	public double totalQueLengthOverTime;
	public double ACBKQueueLengthOverTime;
	public double ACBEQueueLengthOverTime;
	public double ACVIQueueLengthOverTime;
	public double ACVOQueueLengthOverTime;
	int numberOfAC_BK;
	int numberOfAC_BE;
	int numberOfAC_VI;
	int numberOfAC_VO;

	public ServerQoS(int node, int maxBuffer, double rate) {
		super(node, maxBuffer, rate);
		// this.NODE = node;
		// this.rate = rate;
		this.edcaModule = new EDCAmodule(node, maxBuffer);
	}

	/***
	 * Use for another event invalidates this server's AIFS event (or error
	 * occurs on transmission (timeout b/c of collision)
	 * 
	 * Value of AIFS depend on priority of packet
	 * 
	 * @param old
	 * @param newTime
	 */
	public void backOff(Event old, double newTime) {

		/*
		 * creates a backOff check event between 0 and SimMain.Min*2^backOff
		 * SLOT_TIMEs away this backoff countdown will only count down when the
		 * channels is idle for DIFS/EIFS does not check for channel idleness,
		 * that is the BACKOFF_CHECK eventtypes' job
		 */
		double tmpbackoff = edcaModule.CWminDict.get(old.acType)
				* Math.pow(2, backOff);
		if (tmpbackoff > edcaModule.CWmaxDict.get(old.acType)) {
			tmpbackoff = (int) edcaModule.CWmaxDict.get(old.acType);
		}

		lastFrame = old;
		lastCountDownTime = newTime;

		backOffCountDown = Math.random() * tmpbackoff * SimMain.SLOT_TIME;
		// add to waitinglist
		if (!Event.waitingOnChannel.contains(this))
			Event.waitingOnChannel.add(this);

		backOff++;
	}

	/***
	 * Process event
	 * 
	 * @param e
	 * @param rate
	 * @param max_buffer
	 * @throws Exception
	 */
	public void processEvent(Event e) throws Exception {
		totalQueLengthOverTime += (e.time - timeAtLastEvent)
				* edcaModule.getTotalQueueLength();
		ACBKQueueLengthOverTime += (e.time - timeAtLastEvent)
				* edcaModule.getACQueueLength(EventACType.AC_BK);
		ACBEQueueLengthOverTime += (e.time - timeAtLastEvent)
				* edcaModule.getACQueueLength(EventACType.AC_BE);
		ACVIQueueLengthOverTime += (e.time - timeAtLastEvent)
				* edcaModule.getACQueueLength(EventACType.AC_VI);
		ACVOQueueLengthOverTime += (e.time - timeAtLastEvent)
				* edcaModule.getACQueueLength(EventACType.AC_VO);
		timeAtLastEvent = e.time;

		switch (e.type) {
		case FRAME_ARRIVAL:
			edcaModule.processArrivalEvent(e);
			break;
		case CS_FRAME_ACK_SIFS:
			/*
			 * Don't need the invalidate, it invalidates nothing right now b/c
			 * only TRANS_DELAY of time
			 */
			double newTime1 = SimMain.SIFS + e.time + SimMain.TRANS_DELAY;
			double newChannelBusyStart = SimMain.SIFS + e.time;
			Event newEvent1 = new Event(e, newTime1, NODE,
					EventType.ACK_TRANSMITTED, newChannelBusyStart);
			Event.insertAndInvalidate(newEvent1);
			break;
		case CS_FRAME_TRANSMIT_DIFS:
			/*
			 * done! will have been idle b/c no other events invalidated this
			 * DIFS event
			 */
			double newTime2 = e.frameLength * 8 / rate + e.time;
			Event newEvent2 = new Event(e, newTime2, NODE,
					EventType.FRAME_TRANSMITTED, e.time);
			Event.insertAndInvalidate(newEvent2);

			// for reporting
			usedTime += newTime2;
			break;
		case BACKOFF_DONE:
			// send the packet
			double newTime3 = e.frameLength * 8 / rate + e.time;
			Event newEvent3 = new Event(e, newTime3, NODE,
					EventType.FRAME_TRANSMITTED, e.time);
			Event.insertAndInvalidate(newEvent3);

			// for reporting
			usedTime += newTime3;

			Event.waitingOnChannel.remove(this);
			break;
		case CHANNEL_IDLE_DIFS_EIFS:
			Event.startCountDown(e.time);
			break;
		case ACK_TRANSMITTED:
			countNumberOfPacket(e);
			edcaModule.internalBackOff(e);
			break;
		case FRAME_TRANSMITTED:
			processTransmittedEvent(e, rate);
			break;
		default:
			throw new Exception("no eventtype");
		}
	}

	/***
	 * Count number of transmitted packet by access category type
	 * 
	 * @param e
	 */
	private void countNumberOfPacket(Event e) {
		switch (e.acType) {
		case AC_BK:
			numberOfAC_BK++;
			break;
		case AC_BE:
			numberOfAC_BE++;
			break;
		case AC_VI:
			numberOfAC_VI++;
			break;
		case AC_VO:
			numberOfAC_VO++;
			break;
		}
	}

	/**
	 * Get byte transmitted of this server
	 * 
	 * @return byte transmitted
	 */
	public double getBytesTransmittedSuccessfully() {
		return edcaModule.getBytesTransmittedSuccessfully();
	}

	/**
	 * Get packet delay of this server
	 * 
	 * @return
	 */
	public double getPacketDelay() {
		return edcaModule.getPacketDelay();
	}
	
	/**
	 * Get number of packet transmitted successfully
	 * 
	 * @return
	 */
	public double getFramesTransmittedSuccessfully() {
		return edcaModule.getFramesTransmittedSuccessfully();
	}

	public int getTotalQueueLength() {
		return edcaModule.getTotalQueueLength();
	}

	public int getACBKQueueLength() {
		return edcaModule.getACQueueLength(EventACType.AC_BK);
	}

	public int getACBEQueueLength() {
		return edcaModule.getACQueueLength(EventACType.AC_BE);
	}

	public int getACVIQueueLength() {
		return edcaModule.getACQueueLength(EventACType.AC_VI);
	}

	public int getACVOQueueLength() {
		return edcaModule.getACQueueLength(EventACType.AC_VO);
	}

	public int getTotalFramesDropped() {
		return edcaModule.getTotalFramesDropped();
	}

	public int getACBKFramesDropped() {
		return edcaModule.getFrameDropped(EventACType.AC_BK);
	}

	public int getACBEFramesDropped() {
		return edcaModule.getFrameDropped(EventACType.AC_BE);
	}

	public int getACVIFramesDropped() {
		return edcaModule.getFrameDropped(EventACType.AC_VI);
	}

	public int getACVOFramesDropped() {
		return edcaModule.getFrameDropped(EventACType.AC_VO);
	}

}
