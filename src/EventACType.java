public enum EventACType {
	AC_BK(0), // Background (Low Priority)
	AC_BE(1), // Best Effort
	AC_VI(2), // Video
	AC_VO(3); // Voice (High Priority)

	private final int value;

	private EventACType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	/***
	 * Random EventACType
	 * 
	 * @return EventACType enum
	 */
	public static EventACType getRandom() {
		return values()[(int) (Math.random() * values().length)];
	}
}
