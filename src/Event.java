
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class Event implements Comparable<Event> {
	public static PriorityQueue<Event> GEL;
	public static LinkedList<Server> waitingOnChannel; 
	
	//only used for events that use the channel (FRAME TRANSMITTED and ACK TRANSMITTED, otherwise both 0
	public double channelBusyStart;
	public double channelBusyUntil;
	
	public double time,arrivalTime;
	
	//which node owns this event
	public int node;
	
	public EventType type;
	public EventACType acType;
	
	 //range from 64-1500 bytes
	public int frameLength;
	
	//destination node
	public int frameDestNode;
	
	public Event(double newTime,int newNode,int newFrameLength, int newFrameDestNode, EventType newType) {
		time = newTime;
		type = newType;
		node=newNode;
		frameLength=newFrameLength;
		frameDestNode=newFrameDestNode;
		acType = null;
	}
	
	public Event(Event parent,double newTime,int newNode,EventType newType)
	{
		time = newTime;
		type = newType;
		node=newNode;
		arrivalTime=parent.arrivalTime;
		frameLength=parent.frameLength;
		frameDestNode=parent.frameDestNode;
		acType = parent.acType;
	}
	
	public Event(Event parent,double newTime,int newNode,EventType newType,double newChannelBusyStart)
	{
		time = newTime;
		type = newType;
		node=newNode;
		arrivalTime=parent.arrivalTime;
		frameLength=parent.frameLength;
		frameDestNode=parent.frameDestNode;
		channelBusyStart=newChannelBusyStart;
		channelBusyUntil=time;
		acType = parent.acType;
	}
	
	public Event(double newTime,int newNode,int newFrameLength, int newFrameDestNode, EventType newType, EventACType newACType) {
		time = newTime;
		type = newType;
		node=newNode;
		frameLength=newFrameLength;
		frameDestNode=newFrameDestNode;
		acType = newACType;
	} 

	/***
	 * Init GEL and waiting on channel list
	 */
	public static void init() 
	{
		GEL = new PriorityQueue<Event>();
		waitingOnChannel=new LinkedList<Server>();
	}

	/***
	 * Inserting event to GEL
	 * 
	 * @param newEvent
	 */
	public static void insert(Event newEvent) 
	{
		//O(log n) time
		GEL.add(newEvent);	
	}
	
	/***
	 * Look for any channel activity between startTime and endTime (inclusive)
	 * (must look ahead for FRAME_TRANSMITTED or ACK_TRANSMITTED
	 * and check range to see if in this range times can be equal, must still
	 * work!
	 * 
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public static boolean checkIfIdle(double startTime,double endTime)
	{
		for(Event tmp:Event.GEL)
		{
			if(tmp.type==EventType.FRAME_TRANSMITTED||tmp.type==EventType.ACK_TRANSMITTED)
			{//then frame is a "channel is/was busy" frame
				if(tmp.channelBusyStart<=endTime+SimMain.TRANS_DELAY&&tmp.channelBusyUntil>=startTime-SimMain.TRANS_DELAY)
				{
					return false;
				}
				else if(tmp.channelBusyStart<=startTime-SimMain.TRANS_DELAY&&tmp.channelBusyUntil>=endTime+SimMain.TRANS_DELAY)
				{
					return false;
				}
			}
		}
		return true;
	}
	
	/***
	 * Transmitting event, invalidate any DIFS during the event
	 * 
	 * @param newEvent
	 */
	public static void insertAndInvalidate(Event newEvent)
	{
		Iterator<Event> it = Event.GEL.iterator();
		Event tmp;
		while (it.hasNext())
		{
			tmp =it.next();
			if((tmp.type == EventType.CS_FRAME_TRANSMIT_DIFS||tmp.type == EventType.CHANNEL_IDLE_DIFS_EIFS) &&
					tmp.time > newEvent.channelBusyStart+SimMain.TRANS_DELAY && 
					tmp.time < newEvent.channelBusyUntil+SimMain.TRANS_DELAY)
			{
				if(tmp.type!=EventType.CHANNEL_IDLE_DIFS_EIFS)
					SimMain.servers[tmp.node].backOff(tmp, newEvent.time + SimMain.TRANS_DELAY);
				it.remove();
			}
			else if (tmp.type == EventType.BACKOFF_DONE)
			{
				if(tmp.time > newEvent.channelBusyStart + SimMain.TRANS_DELAY)
				{
					SimMain.servers[tmp.node].backOffCountDown -= 
							(newEvent.channelBusyStart+SimMain.TRANS_DELAY - SimMain.servers[tmp.node].lastCountDownTime);
					it.remove();
				}
			}
		}
		
		insert(newEvent);
	}
	
	/***
	 * Called after the channel has been idel for DIFS/EIFS amount of time
	 * 
	 * @param time
	 */
	public static void startCountDown(double time) 
	{
		for(Server s:waitingOnChannel) 
		{
			s.lastCountDownTime = time;
			Event tmpAdd = new Event(s.lastFrame, time + s.backOffCountDown, s.NODE, EventType.BACKOFF_DONE);
			Event.insert(tmpAdd);
		}
	}

	/***
	 * Compare event, event with the least time is "greatest"
	 */
	@Override
	public int compareTo(Event arg)  
	{
		if(arg.time<this.time)
			return 1;
		if(arg.time>this.time)
			return -1;
		return 0;
	}

}
