import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class EDCAmodule {

	private int maxbuffer;
	private int node;

	public Map<EventACType, Integer> CWminDict;
	public Map<EventACType, Integer> CWmaxDict;
	private Map<EventACType, Integer> AIFSNDict;
	public Map<EventACType, Double> AIFS;

	private Map<EventACType, LinkedList<Event>> acQueue;

	private Map<EventACType, Integer> que;
	private Map<EventACType, Integer> framesDropped;

	private double bytesTransmittedSuccessfully;
	private double packetDelay;
	private double framesTransmittedSuccessfully;

	/***
	 * 
	 * @param node
	 * @param maxbuffer
	 */
	@SuppressWarnings("serial")
	public EDCAmodule(int node, int maxbuffer) {
		this.node = node;
		this.maxbuffer = maxbuffer;
		this.packetDelay = 0;
		this.framesTransmittedSuccessfully = 0;

		CWminDict = new HashMap<EventACType, Integer>() {
			{
				// Default CWmin for EDCA
				// Ref
				// http://en.wikipedia.org/wiki/IEEE_802.11e-2005#Enhanced_distributed_channel_access_.28EDCA.29
				put(EventACType.AC_BK, 15);
				put(EventACType.AC_BE, 15);
				put(EventACType.AC_VI, 7);
				put(EventACType.AC_VO, 3);
			}
		};

		CWmaxDict = new HashMap<EventACType, Integer>() {
			{
				// Default CWmax for EDCA
				// Ref
				// http://en.wikipedia.org/wiki/IEEE_802.11e-2005#Enhanced_distributed_channel_access_.28EDCA.29
				put(EventACType.AC_BK, 1023);
				put(EventACType.AC_BE, 1023);
				put(EventACType.AC_VI, 15);
				put(EventACType.AC_VO, 7);
			}
		};

		AIFSNDict = new HashMap<EventACType, Integer>() {
			{
				put(EventACType.AC_BK, 7);
				put(EventACType.AC_BE, 3);
				put(EventACType.AC_VI, 2);
				put(EventACType.AC_VO, 2);
			}
		};

		AIFS = new HashMap<EventACType, Double>() {
			{
				// AIFSN[AC] * ST + SIFS
				// Ref
				// http://en.wikipedia.org/wiki/Arbitration_inter-frame_spacing
				put(EventACType.AC_BK, AIFSNDict.get(EventACType.AC_BK)
						* SimMain.SLOT_TIME + SimMain.SIFS);
				put(EventACType.AC_BE, AIFSNDict.get(EventACType.AC_BE)
						* SimMain.SLOT_TIME + SimMain.SIFS);
				put(EventACType.AC_VI, AIFSNDict.get(EventACType.AC_VI)
						* SimMain.SLOT_TIME + SimMain.SIFS);
				put(EventACType.AC_VO, AIFSNDict.get(EventACType.AC_VO)
						* SimMain.SLOT_TIME + SimMain.SIFS);
			}
		};

		acQueue = new HashMap<EventACType, LinkedList<Event>>() {
			{
				put(EventACType.AC_BK, new LinkedList<Event>());
				put(EventACType.AC_BE, new LinkedList<Event>());
				put(EventACType.AC_VI, new LinkedList<Event>());
				put(EventACType.AC_VO, new LinkedList<Event>());
			}
		};

		que = new HashMap<EventACType, Integer>() {
			{
				put(EventACType.AC_BK, 0);
				put(EventACType.AC_BE, 0);
				put(EventACType.AC_VI, 0);
				put(EventACType.AC_VO, 0);
			}
		};

		framesDropped = new HashMap<EventACType, Integer>() {
			{
				put(EventACType.AC_BK, 0);
				put(EventACType.AC_BE, 0);
				put(EventACType.AC_VI, 0);
				put(EventACType.AC_VO, 0);
			}
		};

	}

	/***
	 * Process arrival event and put it in Access Category Queue
	 * 
	 * @param e
	 */
	public void processArrivalEvent(Event e) {
				
		if (que.get(EventACType.AC_BK) == 0 && que.get(EventACType.AC_BE) == 0
				&& que.get(EventACType.AC_VI) == 0
				&& que.get(EventACType.AC_VO) == 0) {
			// if (this.getTotalQueueLength() == 0) {

			if (Event.checkIfIdle(e.time - SimMain.TRANS_DELAY, e.time
					- SimMain.TRANS_DELAY)) {
				double newTime = e.time + AIFS.get(e.acType);
				Event newEvent = new Event(e, newTime, node,
						EventType.CS_FRAME_TRANSMIT_DIFS);
				Event.insert(newEvent);
			} else {
				SimMain.servers[node].backOff(e, e.time);
			}

			int tmpQue = que.get(e.acType);
			tmpQue++;
			que.put(e.acType, tmpQue);
		} else {
			if (maxbuffer == -1 || maxbuffer > que.get(e.acType)) {
				int tmpQue = que.get(e.acType);
				tmpQue++;
				que.put(e.acType, tmpQue);

				acQueue.get(e.acType).add(e);
			} else {
				int tmpFramesDropped = framesDropped.get(e.acType);
				tmpFramesDropped++;
				framesDropped.put(e.acType, tmpFramesDropped);
			}
		}

	}

	/***
	 * Internal back off for event in 4 AC Queue
	 * 
	 * @param e
	 */
	public void internalBackOff(Event e) {
		// decrement que, frame is GONE! and ACK is received (already delayed!)
		int tmp_queue_length = que.get(e.acType);
		if (tmp_queue_length > 0) {
			tmp_queue_length--;
			que.put(e.acType, tmp_queue_length);
		}

		// reset back-off counter
		SimMain.servers[node].backOff = 0;

		// reporting-add to successfully transmitted frame list
		this.bytesTransmittedSuccessfully += e.frameLength;
		SimMain.bytesTransmittedSuccessfully += e.frameLength;
		SimMain.framesTransmittedSuccessfully++;
		SimMain.totalPacketDelay += e.time - e.arrivalTime - SimMain.SIFS
				- SimMain.TRANS_DELAY;

		// packet delay of this server
		this.packetDelay += e.time - e.arrivalTime - SimMain.SIFS
				- SimMain.TRANS_DELAY;
		// packet transmitted successfully of this server
		this.framesTransmittedSuccessfully++;

		// get first event from 4 queue
		Event eventAC_BK = acQueue.get(EventACType.AC_BK).peekFirst();
		Event eventAC_BE = acQueue.get(EventACType.AC_BE).peekFirst();
		Event eventAC_VI = acQueue.get(EventACType.AC_VI).peekFirst();
		Event eventAC_VO = acQueue.get(EventACType.AC_VO).peekFirst();

		if (eventAC_BK != null || eventAC_BE != null || eventAC_VI != null
				|| eventAC_VO != null) {

			// precompute event time of 4 event from e.time + AIFS[EventACType]
			// + backOffCountDown
			double eventTimeAC_BK = 0;
			double eventTimeAC_BE = 0;
			double eventTimeAC_VI = 0;
			double eventTimeAC_VO = 0;

			if (eventAC_BK != null) {
				eventTimeAC_BK = e.time + AIFS.get(EventACType.AC_BK)
						+ calculateBackOffCountDown(EventACType.AC_BK);
			}

			if (eventAC_BE != null) {
				eventTimeAC_BE = e.time + AIFS.get(EventACType.AC_BE)
						+ calculateBackOffCountDown(EventACType.AC_BE);
			}

			if (eventAC_VI != null) {
				eventTimeAC_VI = e.time + AIFS.get(EventACType.AC_VI)
						+ calculateBackOffCountDown(EventACType.AC_VI);
			}

			if (eventAC_VO != null) {
				eventTimeAC_VO = e.time + AIFS.get(EventACType.AC_VO)
						+ calculateBackOffCountDown(EventACType.AC_VO);
			}

			Event selectedEvent = null;
			EventACType selectedACType = null;
			double tmp_time = Double.MAX_VALUE;

			// compare event time
			if (eventTimeAC_BK > 0 && eventTimeAC_BK <= tmp_time) {
				selectedEvent = eventAC_BK;
				selectedACType = EventACType.AC_BK;
			}

			if (eventTimeAC_BE > 0 && eventTimeAC_BE <= tmp_time) {
				selectedEvent = eventAC_BE;
				selectedACType = EventACType.AC_BE;
			}

			if (eventTimeAC_VI > 0 && eventTimeAC_VI <= tmp_time) {
				selectedEvent = eventAC_VI;
				selectedACType = EventACType.AC_VI;
			}

			if (eventTimeAC_VO > 0 && eventTimeAC_VO <= tmp_time) {
				selectedEvent = eventAC_VO;
				selectedACType = EventACType.AC_VO;
			}

			// update lastFrame, backOffCountDown, lastCountDownTime in server
			SimMain.servers[node].lastFrame = selectedEvent;
			SimMain.servers[node].lastCountDownTime = e.time;
			SimMain.servers[node].backOffCountDown = calculateBackOffCountDown(selectedACType);

			// remove first event that used for backoff from queue
			acQueue.get(selectedACType).removeFirst();

			// add server to waitinglist
			if (!Event.waitingOnChannel.contains(SimMain.servers[node]))
				Event.waitingOnChannel.add(SimMain.servers[node]);

			SimMain.servers[node].backOff++;
		}

		Event.insert(new Event(e.time + AIFS.get(e.acType), 0, 0, 0,
				EventType.CHANNEL_IDLE_DIFS_EIFS));
	}

	/***
	 * Calculate back off count down time
	 * 
	 * @param acType
	 * @return backOffCountDown time
	 */
	private double calculateBackOffCountDown(EventACType acType) {

		double tmpbackoff = CWminDict.get(acType)
				* Math.pow(2, SimMain.servers[node].backOff);

		if (tmpbackoff > CWmaxDict.get(acType)) {
			tmpbackoff = (int) CWmaxDict.get(acType);
		}

		return Math.random() * tmpbackoff * SimMain.SLOT_TIME;
	}

	/***
	 * Get event from the head (first element) of queue
	 * 
	 * @param acType
	 *            - EventACType
	 * @return event object
	 */
	public Event getEvent(EventACType acType) {
		return acQueue.get(acType).poll();
	}

	/***
	 * Get total frames dropped from server
	 * 
	 * @return sum of frames dropped from each queue
	 */
	public int getTotalFramesDropped() {
		return framesDropped.get(EventACType.AC_BK)
				+ framesDropped.get(EventACType.AC_BE)
				+ framesDropped.get(EventACType.AC_VI)
				+ framesDropped.get(EventACType.AC_VO);
	}

	/***
	 * Get frames dropped from queue
	 * 
	 * @param acType
	 * @return number of frames dropped
	 */
	public int getFrameDropped(EventACType acType) {
		return framesDropped.get(acType);
	}

	/***
	 * Get total queue length of server
	 * 
	 * @return sum of queue length from each queue
	 */
	public int getTotalQueueLength() {
		return que.get(EventACType.AC_BK) + que.get(EventACType.AC_BE)
				+ que.get(EventACType.AC_VI) + que.get(EventACType.AC_VO);
	}

	/***
	 * Get queue length from EventACType
	 * 
	 * @param acType
	 * @return queue length
	 */
	public int getACQueueLength(EventACType acType) {
		return que.get(acType);
	}

	/**
	 * Get byte transmitted of this server
	 * 
	 * @return byte transmitted
	 */
	public double getBytesTransmittedSuccessfully() {
		return bytesTransmittedSuccessfully;
	}

	/**
	 * Get packet delay of this server
	 * 
	 * @return
	 */
	public double getPacketDelay() {
		return packetDelay;
	}

	/**
	 * Get number of packet transmitted successfully
	 * 
	 * @return
	 */
	public double getFramesTransmittedSuccessfully() {
		return framesTransmittedSuccessfully;
	}

}
