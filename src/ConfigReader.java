import java.io.FileInputStream;
import java.util.Properties;

public class ConfigReader {

	private static final String NORMAL_NODE = "normal";
	private static final String QoS_NODE = "qos";
	private static final String MONITOR_NODE = "monitor";

	private static ConfigReader instance;

	private Properties prop;

	private SimulationType simulationType;
	private int endCondition;
	private int randomSeed;
	private DistributionType distributionType;
	private double rate;

	private double normalLambda;
	private int normalNodeNumber;
	private int normalBufferSize;
	private int normalPriorityType;

	private double qosLambda;
	private int qosNodeNumber;
	private int qosBufferSize;
	private int qosPriorityType;

	private double monitorLambda;
	private int monitorNodeNumber;
	private int monitorBufferSize;
	private int monitorPriorityType;

	public static ConfigReader getInstance() throws Exception {
		if (instance == null)
			instance = new ConfigReader();

		return instance;
	}

	private ConfigReader() throws Exception {
		prop = new Properties();

		loadConfig();
	}

	private void loadConfig() throws Exception {
		prop.load(new FileInputStream(SimMain.CONFIG_FILE));

		loadSimulationType();
		loadEndCondition();
		loadRandomSeed();
		loadDistributionType();
		loadRate();

		loadLambda(ConfigReader.NORMAL_NODE);
		loadNode(ConfigReader.NORMAL_NODE);
		loadBuffer(ConfigReader.NORMAL_NODE);
		loadPriorityType(ConfigReader.NORMAL_NODE);

		loadLambda(ConfigReader.QoS_NODE);
		loadNode(ConfigReader.QoS_NODE);
		loadBuffer(ConfigReader.QoS_NODE);
		loadPriorityType(ConfigReader.QoS_NODE);

		loadLambda(ConfigReader.MONITOR_NODE);
		loadNode(ConfigReader.MONITOR_NODE);
		loadBuffer(ConfigReader.MONITOR_NODE);
		loadPriorityType(ConfigReader.MONITOR_NODE);

	}

	private void loadSimulationType() throws Exception {
		String sim_type = prop.getProperty("Simulation_Type", "time");
		if (sim_type.equalsIgnoreCase("time"))
			simulationType = SimulationType.TIME;
		else if (sim_type.equalsIgnoreCase("frame"))
			simulationType = SimulationType.FRAME;
		else
			throw new Exception("Invalid simulation type!");
	}

	private void loadEndCondition() {
		endCondition = Integer.parseInt(prop
				.getProperty("End_Condition", "100").trim());
	}

	private void loadRandomSeed() throws Exception {
		randomSeed = Integer.parseInt(prop.getProperty("Random_Seed", "1")
				.trim());
	}

	private void loadDistributionType() throws Exception {
		String dis_type = prop.getProperty("Distribution_Type", "nedt").trim();
		if (dis_type.equalsIgnoreCase("nedt"))
			distributionType = DistributionType.NEDT;
		else if (dis_type.equalsIgnoreCase("pdt"))
			distributionType = DistributionType.PDT;
		else
			throw new Exception("Invalid distribution type!");
	}

	private void loadRate() throws Exception {
		rate = Double.parseDouble(prop.getProperty("rate", "0.0"));
	}

	private void loadLambda(String node) throws Exception {
		double lambda = Double.parseDouble(prop.getProperty(node + ".lambda",
				"0.0"));

		if (node.equals(ConfigReader.NORMAL_NODE)) {
			this.normalLambda = lambda;
		} else if (node.equals(ConfigReader.QoS_NODE)) {
			this.qosLambda = lambda;
		} else if (node.equals(ConfigReader.MONITOR_NODE)) {
			this.monitorLambda = lambda;
		}
	}

	private void loadNode(String node) throws Exception {
		int numNodes = Integer.parseInt(prop.getProperty(node + ".numNodes",
				"0"));

		if (node.equals(ConfigReader.NORMAL_NODE)) {
			this.normalNodeNumber = numNodes;
		} else if (node.equals(ConfigReader.QoS_NODE)) {
			this.qosNodeNumber = numNodes;
		} else if (node.equals(ConfigReader.MONITOR_NODE)) {
			this.monitorNodeNumber = numNodes;
		}
	}

	private void loadBuffer(String node) throws Exception {
		int bufferSize = Integer.parseInt(prop.getProperty(node
				+ ".Buffer_Size", "0"));

		if (node.equals(ConfigReader.NORMAL_NODE)) {
			this.normalBufferSize = bufferSize;
		} else if (node.equals(ConfigReader.QoS_NODE)) {
			this.qosBufferSize = bufferSize;
		} else if (node.equals(ConfigReader.MONITOR_NODE)) {
			this.monitorBufferSize = bufferSize;
		}

	}

	private void loadPriorityType(String node) throws Exception {
		int priority = Integer.parseInt(prop.getProperty(node + ".priority",
				"-1"));

		if (node.equals(ConfigReader.NORMAL_NODE)) {
			this.normalPriorityType = priority;
		} else if (node.equals(ConfigReader.QoS_NODE)) {
			this.qosPriorityType = priority;
		} else if (node.equals(ConfigReader.MONITOR_NODE)) {
			this.monitorPriorityType = priority;
		}
	}

	public SimulationType getSimulationType() {
		return simulationType;
	}

	public int getEndCondition() {
		return endCondition;
	}

	public int getRandomSeed() {
		return randomSeed;
	}

	public DistributionType getDistributionType() {
		return distributionType;
	}
	
	public double getRate() {
		return rate;
	}

	public double getNormalLambda() {
		return normalLambda;
	}

	public int getNormalNodeNumber() {
		return normalNodeNumber;
	}

	public int getNormalBufferSize() {
		return normalBufferSize;
	}

	public int getNormalPriorityType() {
		return normalPriorityType;
	}

	public double getQosLambda() {
		return qosLambda;
	}

	public int getQosNodeNumber() {
		return qosNodeNumber;
	}

	public int getQosBufferSize() {
		return qosBufferSize;
	}

	public int getQosPriorityType() {
		return qosPriorityType;
	}

	public double getMonitorLambda() {
		return monitorLambda;
	}

	public int getMonitorNodeNumber() {
		return monitorNodeNumber;
	}

	public int getMonitorBufferSize() {
		return monitorBufferSize;
	}

	public int getMonitorPriorityType() {
		return monitorPriorityType;
	}

}
