
public enum DistributionType {
	NEDT("Negative Exponential Distribution"),
	PDT("Pareto Distribution");
	
	private final String name;

	private DistributionType(String s) {
		name = s;
	}

	public String toString() {
		return name;
	}
}
