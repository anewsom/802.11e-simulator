
import java.util.LinkedList;

public class Server {

	public final String NODE_TYPE = "ServerNormal";
	
	protected double rate;
	protected int maxbuffer;
	
	// node number of this server
	public final int NODE;
	
	public double usedTime;
	public double timeAtLastEvent;
	public double totalQueLengthOverTime;
	
	//backedOff says how many times the server has backed off so far
	public int backOff;
	public Event lastFrame;
	public double backOffCountDown;
	public double lastCountDownTime;
	
	int que;
	int framesDropped;
	
	double bytesTransmittedSuccessfully;
	double packetDelay;
	double framesTransmittedSuccessfully;
	
	LinkedList<Event> q;
	
	public Server(int node, int maxBuffer, double rate)
	{
		NODE = node;
		this.rate = rate;
		this.maxbuffer = maxBuffer;
		que=0;
		q=new LinkedList<Event>();
		
		packetDelay = 0;
		framesTransmittedSuccessfully = 0;
	}
	
	/***
	 * Use for another event invalidates this server's DIFS event (or error
	 * occurs on transmission (timeout b/c of collision)
	 * 
	 * @param old
	 * @param newTime
	 */
	public void backOff(Event old,double newTime)
	{
		
		//creates a backOff check event between 0 and SimMain.Min*2^backOff SLOT_TIMEs away
		//this backoff countdown will only count down when the channels is idle for DIFS/EIFS
		//does not check for channel idleness, that is the BACKOFF_CHECK eventtypes' job
		
		double tmpbackoff= SimMain.MIN_BACKOFF*Math.pow(2,backOff);
		if(tmpbackoff>SimMain.MAX_BACKOFF)
		{
			tmpbackoff=(int)SimMain.MAX_BACKOFF;
		}
		
		lastFrame=old;
		lastCountDownTime=newTime;
		
		backOffCountDown = Math.random()*tmpbackoff*SimMain.SLOT_TIME;
		//add to waitinglist
		if (!Event.waitingOnChannel.contains(this))
			Event.waitingOnChannel.add(this);
		
		backOff++;
	}
	
	/***
	 * Process event
	 * 
	 * @param e
	 * @param rate
	 * @param max_buffer
	 * @throws Exception
	 */
	public void processEvent(Event e) throws Exception
	{
		totalQueLengthOverTime += (e.time - timeAtLastEvent) * que;
		timeAtLastEvent = e.time;
		
		switch(e.type)
		{
		case FRAME_ARRIVAL:
			processArrivalEvent(e, maxbuffer);
			break;
		case CS_FRAME_ACK_SIFS:
			//Don't need the invalidate, it invalidates nothing right now b/c only TRANS_DELAY of time
			Event.insertAndInvalidate(new Event(e,SimMain.SIFS + e.time+SimMain.TRANS_DELAY,NODE, EventType.ACK_TRANSMITTED,SimMain.SIFS + e.time));
			break;
		case CS_FRAME_TRANSMIT_DIFS:
			//done! will have been idle b/c no other events invalidated this DIFS event
			double newTime = e.frameLength*8/rate + e.time;
			Event.insertAndInvalidate(new Event(e,newTime,NODE, EventType.FRAME_TRANSMITTED,e.time));
			
			//for reporting
			usedTime += newTime;
			break;
		case BACKOFF_DONE:
			//send the packet
			double newTime2 = e.frameLength*8/rate + e.time;
			Event.insertAndInvalidate(new Event(e,newTime2,NODE, EventType.FRAME_TRANSMITTED,e.time));

			//for reporting
			usedTime += newTime2;
			
			Event.waitingOnChannel.remove(this);
			break;
		case CHANNEL_IDLE_DIFS_EIFS:
			Event.startCountDown(e.time);
			break;
		case ACK_TRANSMITTED:
			//decrement que, frame is GONE! and ACK is received (already delayed!)
			que--;
			
			//reset back-off counter
			backOff=0; 
			
			//reporting-add to successfully transmitted frame list
			this.bytesTransmittedSuccessfully += e.frameLength;
			SimMain.bytesTransmittedSuccessfully+=e.frameLength;
			SimMain.framesTransmittedSuccessfully++;
			SimMain.totalPacketDelay+=e.time-e.arrivalTime-SimMain.SIFS-SimMain.TRANS_DELAY;
			
			// packet delay of this server
			this.packetDelay += e.time - e.arrivalTime - SimMain.SIFS
					- SimMain.TRANS_DELAY;
			// packet transmitted successfully of this server
			this.framesTransmittedSuccessfully++;
			
			//Check for next frame, if so, backoff
			//NOT create DIFS event after waiting 1 slot time, else backoff, else do nothing
			if(que>0)
			{
				backOff(q.remove(), e.time);
			}
			Event.insert(new Event(e.time+SimMain.DIFS,0,0,0,EventType.CHANNEL_IDLE_DIFS_EIFS));
			break;
		case FRAME_TRANSMITTED:
			
			processTransmittedEvent(e,rate);
			break;
		default:
			throw new Exception ("no eventtype");
		}
	}
	
	/***
	 * Process arrival event
	 * 
	 * @param e
	 * @param max_buffer
	 */
	private void processArrivalEvent(Event e, int max_buffer) 
	{

		if (que == 0)
		{
			//then nothing in queue do CS (waits DFS amount of time) DIFS is 28us for 802.11g SIDS is 10us
			if(Event.checkIfIdle(e.time-SimMain.TRANS_DELAY, e.time-SimMain.TRANS_DELAY))
				// put a event for CS-FrameTransmit-DIFS for a start of broadcast DIFS in the future
				Event.insert(new Event (e,e.time+SimMain.DIFS, NODE, EventType.CS_FRAME_TRANSMIT_DIFS));
			else
				backOff(e, e.time);
			
			que++;
		} else
		{
			// server is in use, queue the frame
			int maxque = max_buffer;
			if (maxque == -1 || maxque > que)
			{
				que++;
				q.add(e);
			}
			else
				framesDropped++;
		}
	}
	
	/***
	 * Process transmitted event create CS_ACK for destination node and check
	 * for collisions
	 * 
	 * @param e
	 * @param rate
	 */
	protected void processTransmittedEvent(Event e, double rate) {

		/*
		 * check collisions,meaning check if the channel was busy at all during
		 * broadcast, figure worst case prop delay both ways (-10 start, +10
		 * end)
		 */
		if (!Event.checkIfIdle(e.channelBusyStart - SimMain.TRANS_DELAY,
				e.channelBusyUntil + SimMain.TRANS_DELAY)) {
			// then collision
			SimMain.numberOfCol++;

			/*
			 * we could backoff after timeout, then try again later Timeout
			 * defined as Transdelay*2+Sifs as per the 802.11 documentation
			 */
			backOff(e, e.time + SimMain.TRANS_DELAY * 2 + SimMain.SIFS);

			// insert channel idle event after EIFS
			Event.insert(new Event(e.time + SimMain.EIFS + SimMain.TRANS_DELAY,
					0, 0, 0, EventType.CHANNEL_IDLE_DIFS_EIFS));

		} else {
			/*
			 * no collision, good to go, create cs_ack event SimMain.TRANS_DELAY
			 * in the future
			 */
			Event.insert(new Event(e, e.time + SimMain.TRANS_DELAY, NODE,
					EventType.CS_FRAME_ACK_SIFS));
		}
		usedTime += e.channelBusyUntil - e.channelBusyStart;
	}

}
