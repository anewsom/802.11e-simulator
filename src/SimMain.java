
import java.util.*;

public class SimMain {

	public static final String CONFIG_FILE = "settings.properties";
	
	private static ConfigReader config;
	private static Logger logger;
	
	//used for displaying differently
	private static boolean isPrintExcelFormat = false;
	
	static Random randGen;
	public static double runningTime;
	public static double bytesTransmittedSuccessfully;
	public static double framesTransmittedSuccessfully;
	public static double totalPacketDelay;
	public static double numberOfCol;
	
	public static Server [] servers;
	
	// From 802.11n standard
	static final double DIFS=0.000050;
	static final double SIFS=0.000010;
	static final double TRANS_DELAY=0.000010;
	static final double SLOT_TIME=0.000020;
	static final double MIN_BACKOFF=23;
	static final double MAX_BACKOFF=1021;
	
	// From 802.11 standard if the ack is 0 in size
	static final double EIFS = DIFS + SIFS + TRANS_DELAY;

	public static void main(String[] args) throws Exception {

		config = ConfigReader.getInstance();
		logger = Logger.getInstance();
		
		randGen = new Random(config.getRandomSeed());
		
		Event.init();
		
		initServer();
		
		generateEvent();
		
		run();
		
		logger.printLog();
		
		System.out.println("Done");
	}

	private static void run() throws Exception {
		
		SimulationType simulationType = config.getSimulationType();
		int end_condition = config.getEndCondition();
		
		// now go through the events and process each
		int percent=0;
		Event tmp;
		
		while (true)
		{
			
			if (Event.GEL.isEmpty()) // must be out of events
				break; // experiment over
			tmp = Event.GEL.remove();
			
			runningTime = tmp.time;
			
			if(percent < (int)(tmp.time / end_condition * 50))
			{
				percent++;
				
				if (!isPrintExcelFormat) {
					// print progress bar
					System.out.print("*");
				}
			}
			
			// check for end of experiment
			if (simulationType == SimulationType.TIME && tmp.time > end_condition)
			{
				break; // experiment over
			} 
			else 
			{
				//process each event
				servers[tmp.node].processEvent(tmp);
			}
				
		}
		
		for(Server s:servers)
		{
			if (s instanceof ServerQoS) {
				ServerQoS qos = (ServerQoS)s;
				
				qos.totalQueLengthOverTime += (runningTime - s.timeAtLastEvent) * qos.getTotalQueueLength();
				qos.ACBKQueueLengthOverTime +=  (runningTime - s.timeAtLastEvent) * qos.getACBKQueueLength();
				qos.ACBEQueueLengthOverTime +=  (runningTime - s.timeAtLastEvent) * qos.getACBEQueueLength();
				qos.ACVIQueueLengthOverTime +=  (runningTime - s.timeAtLastEvent) * qos.getACVIQueueLength();
				qos.ACVOQueueLengthOverTime +=  (runningTime - s.timeAtLastEvent) * qos.getACVOQueueLength();
			} else {
				s.totalQueLengthOverTime += (runningTime - s.timeAtLastEvent) * s.que;
			}
		}
		
	}
	
	/***
	 * Initialize server (normal server, qos server, monitor server)
	 */
	private static void initServer() {
		
		int normalMaxBuffer = config.getNormalBufferSize();
		int qosMaxBuffer = config.getQosBufferSize();
		int monitorMaxBuffer = config.getMonitorBufferSize();
		
		int normalNodeNumber = config.getNormalNodeNumber();
		int qosNodeNumber = config.getQosNodeNumber();
		int monitorNodeNumber = config.getMonitorNodeNumber();
		
		int totalNode = normalNodeNumber + qosNodeNumber + monitorNodeNumber;
		
		double rate = config.getRate();
		
		servers= new Server[totalNode];
		
		for(int nodeNumber = 0; nodeNumber < normalNodeNumber; nodeNumber++) {
			servers[nodeNumber] = new Server(nodeNumber, normalMaxBuffer, rate);
		}
		
		for(int nodeNumber = normalNodeNumber; nodeNumber < normalNodeNumber + qosNodeNumber; nodeNumber++) {
			servers[nodeNumber] = new ServerQoS(nodeNumber, qosMaxBuffer, rate);
		}
		
		for(int nodeNumber = normalNodeNumber + qosNodeNumber; nodeNumber < servers.length; nodeNumber++) {
			servers[nodeNumber] = new ServerQoS(nodeNumber, monitorMaxBuffer, rate);
		}
	}
	
	/***
	 * Generate Event to Global Event List
	 * @throws Exception 
	 */
	private static void generateEvent() throws Exception {
		// Init arrival frames for each server
		double initTime = 0;

		SimulationType simulationType = config.getSimulationType();
		DistributionType distributionType = config.getDistributionType();
		int end_condition = config.getEndCondition();
		int normalNodeNumber = config.getNormalNodeNumber();
		int qosNodeNumber = config.getQosNodeNumber();
		int monitorNodeNumber = config.getMonitorNodeNumber();
		
		Event tmpAdd = null;
		EventACType eventACType = null;
		double tmpTime;
		double lambda = 0;
		
		for (Server s:servers)
		{
			if (s.NODE >= 0 && s.NODE < normalNodeNumber){
				lambda = config.getNormalLambda();
				
				// get event priority type from config file
				int priority = config.getNormalPriorityType();
				if (priority == -1) {
					eventACType = EventACType.getRandom();
				} else {
					eventACType = EventACType.values()[priority];
				}
			} else if (s.NODE >= normalNodeNumber && s.NODE < normalNodeNumber + qosNodeNumber) {
				lambda = config.getQosLambda();
				
				// get event priority type from config file				
				int priority = config.getQosPriorityType();
				if (priority == -1) {
					eventACType = EventACType.getRandom();
				} else {
					eventACType = EventACType.values()[priority];
				}
			} else if (s.NODE >= normalNodeNumber + qosNodeNumber && s.NODE < normalNodeNumber + qosNodeNumber + monitorNodeNumber) {
				lambda = config.getMonitorLambda();
				
				// get event priority type from config file				
				int priority = config.getMonitorPriorityType();
				if (priority == -1) {
					eventACType = EventACType.getRandom();
				} else {
					eventACType = EventACType.values()[priority];
				}
			}
			
			initTime=0;
			while (true) 
			{
				// Check distribution function nedt or pdt
				if (distributionType == DistributionType.NEDT)
					tmpTime=nEDT(lambda);
				else if (distributionType == DistributionType.PDT)
					tmpTime=paretoEDT(lambda);
				else
					throw new Exception("Wrong distribution type!");
			
				//this loop chooses a random destination for each frame
				int destNode;
				do
				{
					destNode= (int) (Math.random()*servers.length);
				}while(destNode==s.NODE);
				
				int frameLength = (int)(Math.random() * (1500 - 64) + 64);
				double eventTime = tmpTime + initTime;
				
				tmpAdd = new Event(eventTime, s.NODE, frameLength, destNode, EventType.FRAME_ARRIVAL, eventACType);
				tmpAdd.arrivalTime = eventTime;
				
				// Check simulation type
				if (simulationType == SimulationType.TIME) {
					// if event time > end condition break loop
					if (tmpAdd.time > end_condition)
						break;
				} else if (simulationType == SimulationType.FRAME) {
					// if size of global event list > end condition break loop
					if (Event.GEL.size() > end_condition)
						break;
				}
	
				initTime = tmpAdd.time;
				Event.insert(tmpAdd);
				
			}
		}
	}
	
	/***
	 * Negative exponential distribution
	 * 
	 * @param rate
	 * @return
	 */
	public static double nEDT(double rate) {
		return ((-1 / rate) * Math.log(1 - randGen.nextDouble()));
	}

	/***
	 * Pareto distribution
	 * 
	 * @param rate
	 * @return
	 */
	public static double paretoEDT(double rate) {
		/**
		 * rewriting the fomular E(x) = (alpha*xm)/(alpha-1) for xm we choose
		 * alpha = 3.45 based on the paper
		 * "QoS Analysis of Queuing Systems with Self-Similar Traffic and Heavy-Tailed Packet Sizes"
		 * by Xiaolong Jin and Geyong Min
		 */

		double xm = (((1 / rate) * 2.45) / 3.45);
		return xm * (Math.pow(1 - randGen.nextDouble(), -1 / 3.45));
	}

}
